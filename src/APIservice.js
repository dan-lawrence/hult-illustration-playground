const api = 'graphics.json';

function getAll() {
    return new Promise((resolve, reject) => {
        fetch(api)
            .then(data => data.json())
            .then(data => resolve(data))
            .catch(err => reject(Error(err)))
    });
}

function getItem(id) {
    return new Promise((resolve, reject) => {
       getAll()
           .then(data => {
               const result = data.filter(item => item.id === parseInt(id));
               resolve(result[0])
           })
           .catch(err => reject(Error(`uhoh: ${err}`)));
    });
}

function getSVG(id) {
    return new Promise((resolve, reject) => {
        getItem(id)
            .then(item => {
                fetch(item.path)
                    .then(res => res.text())
                    .then(svg => {
                        resolve(svg)
                    });
            })
            .catch(err => reject(Error(`uhoh: ${err}`)));
    })
}

export default { getAll, getItem, getSVG };


