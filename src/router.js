import Vue from 'vue'
import Router from 'vue-router'
import Editor from '@/components/Editor.vue'
import Home from '@/components/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/edit/:id',
            component: Editor
        }
    ]
})